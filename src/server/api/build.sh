#!/bin/bash

docker build -t a17/tagsapi:build -f Dockerfile.build .

docker create --name extract a17/tagsapi:build
docker cp extract:/app/TagsApi/bin/Release/netcoreapp2.0/publish/ ./app
docker rm -f extract

docker build -t a17/tagsapi -f Dockerfile.dist .