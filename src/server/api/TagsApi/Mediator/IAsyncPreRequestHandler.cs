﻿using System.Threading.Tasks;

namespace TagsApi.Mediator
{
    public interface IAsyncPreRequestHandler<TRequest>
    {
        Task Handle(TRequest request);
    }
}