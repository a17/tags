﻿using System.Threading.Tasks;

namespace TagsApi.Mediator
{
    public interface IAsyncPostRequestHandler<TRequest, TResponse>
    {
        Task Handle(TRequest request, TResponse response);
    }
}