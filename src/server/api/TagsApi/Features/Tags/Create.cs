﻿using System.Threading.Tasks;
using AutoMapper;
using FluentValidation;
using MediatR;
using TagsApi.Database;
using TagsApi.Domain;
using TagsApi.Dtos;

namespace TagsApi.Features.Tags
{
    public class Create
    {
        public class Command : IRequest<TagDto>
        {
            public TagDto TagDto { get; }

            public Command(TagDto tagDto)
            {
                TagDto = tagDto;
            }
        }

        public class CommandValidator : AbstractValidator<Command>
        {
            public CommandValidator()
            {
                RuleFor(c => c.TagDto.Id).Equal(0).WithMessage("Tag id must equal 0");

                RuleFor(c => c.TagDto.Label).NotNull().WithMessage("Tag label must not be null");
                RuleFor(c => c.TagDto.Label).NotEqual(string.Empty).WithMessage("Tag label must not be empty");
            }
        }

        public class CommandHandler : IAsyncRequestHandler<Command, TagDto>
        {
            private readonly TagsDbContext _db;

            private readonly IMapper _mapper;

            public CommandHandler(TagsDbContext db, IMapper mapper)
            {
                _db = db;
                _mapper = mapper;
            }

            public async Task<TagDto> Handle(Command message)
            {
                var tag = _mapper.Map<TagDto, Tag>(message.TagDto);

                _db.Tags.Add(tag);

                await _db.SaveChangesAsync();

                return _mapper.Map<Tag, TagDto>(tag);
            }
        }
    }
}