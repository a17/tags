﻿using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TagsApi.Dtos;

namespace TagsApi.Features.Tags
{
    [Route("/api/tags")]
    public class ApiController : Controller
    {
        private readonly IMediator _mediator;

        public ApiController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpGet]
        public async Task<TagDto[]> Get()
        {
            return await _mediator.Send(new List.Query());
        }

        [HttpPost]
        public async Task<TagDto> Post([FromBody]TagDto dto)
        {
            return await _mediator.Send(new Create.Command(dto));
        }
    }
}