﻿using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using TagsApi.Database;
using TagsApi.Dtos;

namespace TagsApi.Features.Tags
{
    public class List
    {
        public class Query : IRequest<TagDto[]> { }

        public class QueryValidator : AbstractValidator<Query> { }

        public class QueryHandler : IAsyncRequestHandler<Query, TagDto[]>
        {
            private readonly TagsDbContext _db;

            public QueryHandler(TagsDbContext db)
            {
                _db = db;
            }

            public async Task<TagDto[]> Handle(Query message)
            {
                return await _db.Tags
                    .ProjectTo<TagDto>()
                    .ToArrayAsync();
            }
        }
    }
}