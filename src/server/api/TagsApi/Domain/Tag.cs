﻿namespace TagsApi.Domain
{
    public class Tag : IEntity
    {
        public int Id { get; set; }

        public string Label { get; set; }
    }
}