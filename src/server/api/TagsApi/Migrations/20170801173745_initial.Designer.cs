﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TagsApi.Database;

namespace TagsApi.Migrations
{
    [DbContext(typeof(TagsDbContext))]
    [Migration("20170801173745_initial")]
    partial class initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "1.1.2");

            modelBuilder.Entity("TagsApi.Domain.Tag", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("id");

                    b.Property<string>("Label")
                        .IsRequired()
                        .HasColumnName("label")
                        .HasMaxLength(64);

                    b.HasKey("Id");

                    b.ToTable("tags");
                });
        }
    }
}
