﻿using AutoMapper;
using TagsApi.Domain;
using TagsApi.Dtos;

namespace TagsApi.Mapping
{
    public class TagProfile : Profile
    {
        public TagProfile()
        {
            CreateMap<Tag, TagDto>().ReverseMap();
        }
    }
}