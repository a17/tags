﻿namespace TagsApi.Dtos
{
    public class TagDto
    {
        public int Id { get; private set; }

        public string Label { get; private set; }

        public TagDto(int id, string label)
        {
            Id = id;
            Label = label;
        }
    }
}