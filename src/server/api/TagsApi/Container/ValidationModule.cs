﻿using System.Reflection;
using Autofac;
using FluentValidation;
using TagsApi.Features.Tags;
using Module = Autofac.Module;

namespace TagsApi.Container
{
    public class ValidationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var assembly = typeof(List.QueryValidator).GetTypeInfo().Assembly;

            builder.RegisterAssemblyTypes(assembly)
                .Where(t => t.IsClosedTypeOf(typeof(AbstractValidator<>)))
                .AsSelf()
                .InstancePerLifetimeScope();

            builder.RegisterType<List.QueryValidator>()
                .As<AbstractValidator<List.Query>>();

            builder.RegisterType<Create.CommandValidator>()
                .As<AbstractValidator<Create.Command>>();
        }

    }
}