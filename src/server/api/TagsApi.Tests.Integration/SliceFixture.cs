﻿using System;
using System.IO;
using System.Threading.Tasks;
using Autofac;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using TagsApi.Database;
using TagsApi.Domain;

[assembly: Xunit.CollectionBehavior(DisableTestParallelization = true)]

namespace TagsApi.Tests.Integration
{
    public static class SliceFixture
    {
        private static readonly ILifetimeScope LifetimeScope;

        static SliceFixture()
        {
            var hostMock = new Mock<IHostingEnvironment>();

            hostMock.SetupGet(h => h.ContentRootPath).Returns(Directory.GetCurrentDirectory());

            var startup = new Startup(hostMock.Object);

            var services = new ServiceCollection();

            startup.ConfigureServices(services);

            LifetimeScope = startup.ApplicationContainer.Resolve<ILifetimeScope>();
        }

        public static async Task ExecuteScopeAsync(Func<ILifetimeScope, Task> action)
        {
            using (var scope = LifetimeScope.BeginLifetimeScope())
            {
                var dbContext = scope.Resolve<TagsDbContext>();

                try
                {
                    dbContext.BeginTransaction();

                    await action(scope);

                    await dbContext.CommitTransactionAsync();
                }
                catch (Exception)
                {
                    dbContext.RollbackTransaction();
                    throw;
                }
            }
        }

        public static async Task<T> ExecuteScopeAsync<T>(Func<ILifetimeScope, Task<T>> action)
        {
            using (var scope = LifetimeScope.BeginLifetimeScope())
            {
                var dbContext = scope.Resolve<TagsDbContext>();

                try
                {
                    dbContext.BeginTransaction();

                    var result = await action(scope);

                    await dbContext.CommitTransactionAsync();

                    return result;
                }
                catch (Exception)
                {
                    dbContext.RollbackTransaction();
                    throw;
                }
            }
        }

        public static Task ExecuteDbContextAsync(Func<TagsDbContext, Task> action)
        {
            return ExecuteScopeAsync(sp => action(sp.Resolve<TagsDbContext>()));
        }

        public static Task<T> ExecuteDbContextAsync<T>(Func<TagsDbContext, Task<T>> action)
        {
            return ExecuteScopeAsync(sp => action(sp.Resolve<TagsDbContext>()));
        }

        public static async Task ResetDb()
        {
            await ExecuteDbContextAsync(async db => await db.Database.ExecuteSqlCommandAsync("truncate table Tags;"));
        }

        public static async Task InsertAsync(params IEntity[] entities)
        {
            await ExecuteDbContextAsync(async db =>
            {
                foreach (var entity in entities)
                {
                    db.Add(entity);
                }

                return await db.SaveChangesAsync();
            });
        }

        public static Task<T> FindAsync<T>(int id)
            where T : class, IEntity
        {
            return ExecuteDbContextAsync(db => db.Set<T>().FindAsync(id));
        }

        public static Task<TResponse> SendAsync<TResponse>(IRequest<TResponse> request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.Resolve<IMediator>();

                return mediator.Send(request);
            });
        }

        public static Task SendAsync(IRequest request)
        {
            return ExecuteScopeAsync(sp =>
            {
                var mediator = sp.Resolve<IMediator>();

                return mediator.Send(request);
            });
        }
    }
}