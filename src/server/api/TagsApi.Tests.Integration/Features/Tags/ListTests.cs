﻿using System.Threading.Tasks;
using FluentAssertions;
using TagsApi.Domain;
using TagsApi.Features.Tags;
using Xunit;
using static TagsApi.Tests.Integration.SliceFixture;

namespace TagsApi.Tests.Integration.Features.Tags
{
    public class ListTests : BaseIntegrationTest
    {
        [Fact]
        public async Task Should_return_all_tags_from_db()
        {
            var tag1 = new Tag {Label = "first"};
            var tag2 = new Tag {Label = "second"};

            await InsertAsync(tag1, tag2);

            var tagDtos = await SendAsync(new List.Query());

            tagDtos.Length.Should().Be(2);

            tagDtos[0].Id.Should().BeGreaterThan(0);
            tagDtos[0].Label.Should().Be("first");

            tagDtos[1].Id.Should().BeGreaterThan(0);
            tagDtos[1].Label.Should().Be("second");
        }
    }
}