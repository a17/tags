using System.Threading.Tasks;
using FluentAssertions;
using TagsApi.Domain;
using TagsApi.Dtos;
using TagsApi.Features.Tags;
using Xunit;
using static TagsApi.Tests.Integration.SliceFixture;

namespace TagsApi.Tests.Integration.Features.Tags
{
    public class CreateTests : BaseIntegrationTest
    {
        [Fact]
        public async Task Should_create_tag_in_db()
        {
            var tagDto = await SendAsync(new Create.Command(new TagDto(0, "test")));

            var tagInDb = await FindAsync<Tag>(tagDto.Id);

            tagInDb.Id.Should().BeGreaterThan(0);
            tagInDb.Id.Should().Be(tagDto.Id);
            tagInDb.Label.Should().Be("test");
        }
    }
}