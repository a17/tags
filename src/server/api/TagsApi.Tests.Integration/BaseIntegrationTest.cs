﻿using System.Threading.Tasks;
using Xunit;

namespace TagsApi.Tests.Integration
{
    public abstract class BaseIntegrationTest : IAsyncLifetime
    {
        public virtual Task InitializeAsync() => SliceFixture.ResetDb();

        public virtual Task DisposeAsync() => Task.FromResult(0);
    }
}