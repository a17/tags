﻿using System.Linq;
using FluentAssertions;
using FluentValidation.Results;

namespace TagsApi.Tests.Unit.Extensions
{
    public static class ValidationResultExtensions
    {
        public static void ShouldHaveErrorMessage(this ValidationResult validationResult, string message)
        {
            validationResult.IsValid.Should().BeFalse();
            validationResult.Errors.Single().ErrorMessage.Should().Be(message);
        }

        public static void ShouldBeValid(this ValidationResult validationResult)
        {
            validationResult.IsValid.Should().BeTrue();
        }
    }
}