﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using TagsApi.Database;
using TagsApi.Domain;
using TagsApi.Features.Tags;
using TagsApi.Mapping;
using TagsApi.Tests.Unit.Env;
using Xunit;

namespace TagsApi.Tests.Unit.Features.Tags
{
    public class ListQueryTests
    {
        [Fact]
        public async Task Should_read_all_tags_from_db()
        {
            var env = new TestEnv
            {
                TagsInDb = new[]
                {
                    new Tag { Id = 1, Label = "Tag1" }, 
                    new Tag { Id = 2, Label = "Tag2" }, 
                }
            };

            var handler = env.Setup();

            var tagDtos = await handler.Handle(new List.Query());

            tagDtos[0].Id.Should().Be(1);
            tagDtos[0].Label.Should().Be("Tag1");

            tagDtos[1].Id.Should().Be(2);
            tagDtos[1].Label.Should().Be("Tag2");
        }

        private class TestEnv : BaseTestEnv<List.QueryHandler>
        {
            public IEnumerable<Tag> TagsInDb { private get; set; }

            protected override void SetupDependencies()
            {
                Mapper.Initialize(c => c.AddProfile(new TagProfile()));

                var dbContext = SetupDbContext();

                dbContext.Tags.AddRange(TagsInDb);

                dbContext.SaveChangesAsync();
            }

            private TagsDbContext SetupDbContext()
            {
                var options = new DbContextOptionsBuilder<TagsDbContext>()
                    .UseInMemoryDatabase("ListTagsQuery")
                    .Options;

                var context = new TagsDbContext(options);

                ProvideDependencyInstance(context);

                return context;
            }
        }
    }
}