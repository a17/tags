﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using TagsApi.Database;
using TagsApi.Domain;
using TagsApi.Dtos;
using TagsApi.Features.Tags;
using TagsApi.Mapping;
using TagsApi.Tests.Unit.Env;
using Xunit;

namespace TagsApi.Tests.Unit.Features.Tags
{
    public class CreateHandlerTests
    {
        [Fact]
        public async Task Should_save_tag_in_db()
        {
            var env = new TestEnv();

            var command = new Create.Command(new TagDto(0, "Test"));

            var handler = env.Setup();

            var result = await handler.Handle(command);

            var options = new DbContextOptionsBuilder<TagsDbContext>()
                .UseInMemoryDatabase("CreateTagHandler")
                .Options;

            Tag tagInDb;

            using (var context = new TagsDbContext(options))
            {
                tagInDb = context.Tags.Single();
            }

            tagInDb.Id.Should().BeGreaterThan(0);
            tagInDb.Label.Should().Be("Test");
        }

        private class TestEnv : BaseTestEnv<Create.CommandHandler>
        {
            protected override void SetupDependencies()
            {
                Mapper.Initialize(c => c.AddProfile(new TagProfile()));

                ProvideDependencyInstance(Mapper.Instance);

                SetupDbContext();
            }

            private TagsDbContext SetupDbContext()
            {
                var options = new DbContextOptionsBuilder<TagsDbContext>()
                    .UseInMemoryDatabase("CreateTagHandler")
                    .Options;

                var context = new TagsDbContext(options);

                ProvideDependencyInstance(context);

                return context;
            }
        }
    }
}