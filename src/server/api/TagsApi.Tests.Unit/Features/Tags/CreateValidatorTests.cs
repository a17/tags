﻿using FluentValidation.Results;
using TagsApi.Dtos;
using TagsApi.Features.Tags;
using TagsApi.Tests.Unit.Extensions;
using Xunit;

namespace TagsApi.Tests.Unit.Features.Tags
{
    public class CreateValidatorTests
    {
        [Fact]
        public void Should_have_validation_error_for_id_not_0()
        {
            var validationResult = Validate(new TagDto(1, "Test"));

            validationResult.ShouldHaveErrorMessage("Tag id must equal 0");
        }

        [Fact]
        public void Should_not_have_validation_error_for_id_0()
        {
            var validationResult = Validate(new TagDto(0, "Test"));

            validationResult.ShouldBeValid();
        }

        [Theory]
        [InlineData("", "Tag label must not be empty")]
        [InlineData(null, "Tag label must not be null")]
        public void Should_have_validation_error_for_label(string label, string errorMessage)
        {
            var validationResult = Validate(new TagDto(0, label));

            validationResult.ShouldHaveErrorMessage(errorMessage);
        }

        [Fact]
        public void Should_not_have_validation_error_for_not_empty_label()
        {
            var validationResult = Validate(new TagDto(0, "Test"));

            validationResult.ShouldBeValid();
        }

        private ValidationResult Validate(TagDto dto)
        {
            return new Create.CommandValidator().Validate(new Create.Command(dto));
        }
    }
}