﻿using System;
using Autofac.Extras.Moq;
using AutoMapper;
using Moq;
using TagsApi.Mapping;

namespace TagsApi.Tests.Unit.Env
{
    public abstract class BaseTestEnv<TSut>
    {
        private readonly AutoMock _autoMock;

        private bool _isSetup;

        protected BaseTestEnv()
        {
            Mapper.Initialize(c => c.AddProfile(new TagProfile()));

            _autoMock = AutoMock.GetLoose();
        }

        public TSut Setup()
        {
            SetupDependencies();

            _isSetup = true;

            return _autoMock.Create<TSut>();
        }

        public T DependencyInstance<T>()
        {
            CheckIsSetup();

            return _autoMock.Create<T>();
        }

        public Mock<T> DependencyMock<T>() where T : class
        {
            CheckIsSetup();

            return _autoMock.Mock<T>();
        }

        protected void ProvideDependencyInstance<T>(T instance) where T : class
        {
            _autoMock.Provide(instance);
        }

        protected abstract void SetupDependencies();

        private void CheckIsSetup()
        {
            if (!_isSetup)
            {
                throw new Exception("Test environment must be setup by calling Setup() method first");
            }
        }
    }
}