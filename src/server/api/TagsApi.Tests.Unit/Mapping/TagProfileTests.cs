using AutoMapper;
using FluentAssertions;
using TagsApi.Domain;
using TagsApi.Dtos;
using TagsApi.Mapping;
using Xunit;

namespace TagsApi.Tests.Unit.Mapping
{
    public class TagProfileTests
    {
        [Fact]
        public void Should_map_from_tag_to_dto()
        {
            var tag = new Tag
            {
                Id = 52,
                Label = "TestTag"
            };

            var tagDto = Mapper.Map<Tag, TagDto>(tag);

            tagDto.Id.Should().Be(52);
            tagDto.Label.Should().Be("TestTag");
        }

        [Fact]
        public void Should_map_from_dto_to_tag()
        {
            var dto = new TagDto(52, "TestTag");

            var tag = Mapper.Map<TagDto, Tag>(dto);

            tag.Id.Should().Be(52);
            tag.Label.Should().Be("TestTag");
        }

        private IMapper Mapper
        {
            get
            {
                var config = new MapperConfiguration(c => c.AddProfile(new TagProfile()));
                return config.CreateMapper();
            }
        }
    }
}
