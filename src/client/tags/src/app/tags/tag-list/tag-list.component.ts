import { Component, OnInit } from '@angular/core';

import { Tag } from '../tag';
import { TagsService } from '../tags.service';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.css'],
  providers: [TagsService]
})
export class TagListComponent implements OnInit {

  tags: Tag[];

  constructor(
    private tagsService: TagsService
  ) { }

  ngOnInit() {
    this.tagsService.getTags()
      .subscribe(tags => this.tags = tags);
  }

}
