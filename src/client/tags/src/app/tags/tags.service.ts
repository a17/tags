import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class TagsService {

  private apiUrl = '/api/tags';

  constructor(
    private http: Http,
  ) { }

  getTags() {
    return this.http.get(this.apiUrl)
      .map((res: Response) => res.json());
  }
}
