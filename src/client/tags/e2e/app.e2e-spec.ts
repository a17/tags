import { TagsPage } from './app.po';

describe('tags App', () => {
  let page: TagsPage;

  beforeEach(() => {
    page = new TagsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
